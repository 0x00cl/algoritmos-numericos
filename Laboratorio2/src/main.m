clc; clear variables; format long;

% Cargar puntos con ruido y asignar ejes correspondientes
puntos = readmatrix('data_points.csv');
xr = puntos(1,:);
yr = puntos(2,:);

% Datos funcion original
xo = 0:0.1:5;
fo = @(x) -x.^2+5.*x-2;
yo = fo(xo);

% Polinomio de Lagrange
xl = 0:0.1:5;
auxfl = interpolacionLagrange(xr,yr);
fl = matlabFunction(auxfl)
yl = fl(xl);

% Grafica funcion con ruido vs polinomio Lagrange
plot(xr,yr,'o',xl,yl,':.')

% Polinomio de Newton
xn = 0:0.1:5;
auxfn = zeros(1,length(xn));
for i =1 : length(xn)-1
    auxfn(i) = newton(xr,yr, xn(i));
end

% Grafica funcion con ruido vs newton
plot(xr,yr,'o',xn,auxfn,':.')

% Calculo diferencia de areas bajo la curva
% Trapecio
areaTrapecioo = trapecio(fo, 0, 5, length(xo));
areaTrapeciol = trapecio(fl, 0, 5, length(xl));
diffAreaTrapecio = areaTrapeciol - areaTrapecioo
% Simpson 1/3
areaSimpson13o = simpson13(fo, 0, 5, length(xo));
areaSimpson13l = simpson13(fl, 0, 5, length(xl));
diffAreaSimpson = areaSimpson13l - areaSimpson13o


% Spline lineal
splines = zeros(1,length(xr));
for i = 1 : length(xr)
    splines(i) = splineLineal(xr,yr,xr(i));
end

% Grafica spline lineal
plot(xr,yr,'o',xo,splines,':.')


% Regresion cuadratica
p = polyfit(xo,yo,2);

x1 = linspace(0,5);
y1 = polyval(p,x1);
figure
plot(xo,yo,'o')
hold on
plot(x1,y1)
hold off