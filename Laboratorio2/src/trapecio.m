function [sol] = trapecio(funcion, limInf, limSup, intervalos)
    sol = [];
    h = (limSup-limInf)/intervalos;
    f = (funcion(limInf)+funcion(limSup))/2;
    for k = 1:intervalos - 1
        x = limInf + h * k;
        f = f+funcion(x);
    end
    f = f*h;
    sol = [sol, f];
end
