function [sol] = simpson13(funcion, limInf, limSup, intervalos)
    sol = [];
    h = (limSup-limInf)/intervalos;
    f = funcion(limInf)+funcion(limSup);
    for k = 1:2:intervalos - 1
        f = f+4*funcion(limInf+k*h);
    end
    for k=2:2:intervalos-2
        f=f+2*funcion(limInf+k*h);
    end
    f = (f*h)/3;
    sol = [sol, f];
end
