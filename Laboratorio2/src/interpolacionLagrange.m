function pol = interpolacionLagrange(x,y)
    sum = 0;
    puntos = length(x);
    syms a
    % Sumatoria
    for i = 1: puntos
        p = 1;
        % Productoria
        for j = 1: puntos
            % Solo si "j" es distinto de "i" se calcula
            % Ya que al ser una division se podria dar que 
            % la resta de abajo de "c" sea 0 y es indeterminado
            if j~=i
                c = (a-x(j))/(x(i)-x(j));
                p = p*c;
            end
        end
        prod = p*y(i);
        sum = sum + prod;
    end
    pol = sum;
end