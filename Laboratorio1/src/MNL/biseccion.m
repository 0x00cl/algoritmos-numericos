function [raices,errores,costo] = biseccion(a,b,tol,f)
	raices = [];
	errores = [];
    costo=0;
	while abs(b-a) > tol
		m = (a+b)/2;
		if f(a)*f(m) < 0
			b = m;
		else
			a = m;
        end 
        costo = costo + 3;
		raices = [raices,m];
		errores = [errores,abs(b-a)];
	end
end