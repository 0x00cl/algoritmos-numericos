function graphNoLinealCosto(secante, biseccion, falsi, newton, fijo)

    figure1 = figure;

    % Create axes
    axes1 = axes('Parent',figure1);
    box(axes1,'on');
    hold(axes1,'all');

    % Create plot
    y = [biseccion secante falsi newton];
    X = categorical({'Bisección', 'Secante', 'Regula Falsi', 'Newton-Raphson'});
    X = reordercats(X,{'Bisección', 'Secante', 'Regula Falsi', 'Newton-Raphson'});
   
    if ~strcmp(fijo, 'NoHayFijo')
        y = [biseccion secante falsi newton fijo];
        X = categorical({'Bisección', 'Secante', 'Regula Falsi', 'Newton-Raphson', 'Punto Fijo'});
        X = reordercats(X,{'Bisección', 'Secante', 'Regula Falsi', 'Newton-Raphson', 'Punto Fijo'});
    end
    bar(X,y);
    title('Tiempo de la función')
    ylabel('Tiempos') % y-axis label
    xlabel('Metodos') % x-axis label
    %legend(['Bisección', 'Secante', 'Regula Falsi', 'Newton-Raphson', 'Punto Fijo'])

end