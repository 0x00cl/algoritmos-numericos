function [raices,errores,costo] = regulaFalsi(a,b,tol,f,iter)
	raices = [];
	errores = [];
    error = 1;
    costo=0;
    for iteracion = 1: iter
        c = b - (f(b) * (b-a)/(f(b)-f(a)));
        costo = costo + 5;
        if(abs(error) < tol)
            break;
        end
        
        if (f(a)*f(c) >= 0)
            error = (a - c) / a;
            a = c;
        else
            error = (b - c) / b;
            b = c;
        end
        raices = [raices,c];
        errores = [errores,abs(error)];
    end
end