function graphNoLinealErrores(secante, biseccion, falsi, newton, fijo)

    figure1 = figure;

    % Create axes
    axes1 = axes('Parent',figure1);
    box(axes1,'on');
    hold(axes1,'all');

    % Create plot
    plot([biseccion],'-o','MarkerSize',2); hold on;
    plot([secante],'-*','MarkerSize',2); hold on;
    plot([falsi],'-p','MarkerSize',2); hold on;
    plot([newton],'-d','MarkerSize',2); hold on;
    legend('Bisección','Secante','Regula Falsi','Newton-Raphson')
    if ~strcmp(fijo, 'NoHayFijo')
        plot([fijo],'-+','MarkerSize',2); hold on;
        legend('Bisección','Secante','Regula Falsi','Newton-Raphson', 'Punto Fijo')
    end
    title('Errores de la función')
    ylabel('Error') % y-axis label
    xlabel('Iter') % x-axis label

end