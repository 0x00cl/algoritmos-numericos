function [raices,errores,costo] = puntoFijo(x0,tol,g)
	raices = [];
	errores = [];
    costo=0;
	xn = x0 + 1;
    while abs(x0-xn) > tol
		xn = x0;
		x0 = g(xn);
        costo = costo + 1;
		raices = [raices,x0];
		errores = [errores,abs(x0-xn)];
    end
end


