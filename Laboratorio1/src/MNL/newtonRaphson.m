function [raices,errores,costo] = newtonRaphson(x0,tol,f,df,iter)
	raices = [];
	errores = [];
	x=x0;
    costo=0;
    for iteraciones = 1 : iter
        if abs(f(x)) <= tol
            break;
        end
        x = x - (f(x)/df(x));
        costo = costo + 3;
        raices = [raices,x];
        errores = [errores,abs(f(x))];
    end
end