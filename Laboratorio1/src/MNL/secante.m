 function [raices,errores,costo] = secante(a, b, tol, f)
    raices = [];
    errores = [];
    costo=0;
	x0 = a;
	x1 = b;
    while abs(x1-x0) > tol
		xn = x1 - ((x1-x0) / (f(x1) - f(x0)))*f(x1);
        costo = costo + 5;
		x1 = x0;
		x0 = xn;
		raices = [raices,xn];
		errores = [errores,abs(x1-x0)];
    end
end