clc; format long;

% ==== DEFINICION FUNCIONES ==== %
f1 = @(x) x - 2^(-x);
df1 = @(x) 2^(-x)*log(2)+1;
g1 = @(x) 2^(-x);

f2 = @(x) exp(-x) - x^2 + 3*x - 2;
df2 = @(x) 3 - exp(-x) - 2*x;
g2 = @(x) (2-exp(-x)+x^2)/3;

f3 = @(x) x^(3) - 7*x^(2) + 14*sin(2*pi*x) - 6;
df3 = @(x) x*(-14 + 3*x) + 28*pi*cos(2*pi*x);
g3 = @(x) x*(-14 + 3*x) + 28*pi*cos(2*pi*x) + x;

% ==== DEFINICION PUNTOS INICIALES ==== %
a = 0;
b = 2;
c = 1;
d = 0.3;
iter = 100;
tol = 10^-16;
x0 = 0;

% ==== FUNCION 1 ==== %
tic
[raicesFijo1,erroresFijo1,costoFijo1] = puntoFijo(x0,tol,g1);
tiempoFijo1 = toc;

tic
[raicesSecante1,erroresSecante1,costoSecante1] = secante(a,b,tol,f1);
tiempoSecante1 = toc;

tic
[raicesBiseccion1,erroresBiseccion1,costoBiseccion1] = biseccion(a,b,tol,f1);
tiempoBiseccion1 = toc;

tic
[raicesFalsi1,erroresFalsi1,costoFalsi1] = regulaFalsi(a,b,tol,f1,iter);
tiempoFalsi1 = toc;

tic
[raicesNewton1,erroresNewton1,costoNewton1] = newtonRaphson(x0,tol,f1,df1,iter);
tiempoNewton1 = toc;

graphNoLineal(raicesSecante1, raicesBiseccion1, raicesFalsi1, raicesNewton1, raicesFijo1)
graphNoLinealErrores(erroresSecante1, erroresBiseccion1, erroresFalsi1, erroresNewton1, erroresFijo1)
graphNoLinealCosto(costoSecante1, costoBiseccion1, costoFalsi1, costoNewton1, costoFijo1)
graphNoLinealTiempo(tiempoSecante1, tiempoBiseccion1, tiempoFalsi1, tiempoNewton1, tiempoFijo1)
% ==== FUNCION 2 ==== %

tic
[raicesFijo2,erroresFijo2,costoFijo2] = puntoFijo(x0,tol,g2) ;
tiempoFijo2 = toc;

tic
[raicesSecante2,erroresSecante2,costoSecante2] = secante(a,c,tol,f2);
tiempoSecante2 = toc;

tic
[raicesFalsi2,erroresFalsi2,costoFalsi2] = regulaFalsi(a,c,tol,f2,iter);
tiempoFalsi2 = toc;

tic
[raicesBiseccion2,erroresBiseccion2,costoBiseccion2] = biseccion(a,c,tol,f2);
tiempoBiseccion2 = toc;

tic
[raicesNewton2,erroresNewton2,costoNewton2] = newtonRaphson(x0,tol,f2,df2,iter);
tiempoNewton2 = toc;

graphNoLineal(raicesSecante2, raicesBiseccion2, raicesFalsi2, raicesNewton2, raicesFijo2)
graphNoLinealErrores(erroresSecante2, erroresBiseccion2, erroresFalsi2, erroresNewton2, erroresFijo2)
graphNoLinealCosto(costoSecante2, costoBiseccion2, costoFalsi2, costoNewton2, costoFijo2)
graphNoLinealTiempo(tiempoSecante2, tiempoBiseccion2, tiempoFalsi2, tiempoNewton2, tiempoFijo2)
% ==== FUNCION 3 ==== %
tic
[raicesX,erroresX,costoX] = puntoFijo(x0,tol,g3) 
toc

tic
[raicesSecante3,erroresSecante3,costoSecante3] = secante(a,d,tol,f3);
tiempoSecante3 = toc;

tic
[raicesFalsi3,erroresFalsi3,costoFalsi3] = regulaFalsi(a,d,tol,f3,iter);
tiempoFalsi3 = toc;

tic
[raicesBiseccion3,erroresBiseccion3,costoBiseccion3] = biseccion(a,d,tol,f3);
tiempoBiseccion3 = toc;

tic
[raicesNewton3,erroresNewton3,costoNewton3] = newtonRaphson(x0,tol,f3,df3,iter);
tiempoNewton3 = toc;

graphNoLineal(raicesSecante3, raicesBiseccion3, raicesFalsi3, raicesNewton3, 'NoHayFijo')
graphNoLinealErrores(erroresSecante3, erroresBiseccion3, erroresFalsi3, erroresNewton3, 'NoHayFijo')
graphNoLinealTiempo(tiempoSecante3, tiempoBiseccion3, tiempoFalsi3, tiempoNewton3, 'NoHayFijo')
graphNoLinealCosto(costoSecante3, costoBiseccion3, costoFalsi3, costoNewton3, 'NoHayFijo')