function [raices,error,costo] = Housholder(A,b)
    [m, n] = size(A);
    Q = eye(m);      
    R = A;
    costo = 0;
    for i = 1: n 
        normx = norm(R(i:end,i));
        s     = -sign(R(i,i));
        u1    = R(i,i) - s*normx;
        w     = R(i:end,i)/u1;
        w(1)  = 1;
        tau   = -s*u1/normx;
        R(i:end,:) = R(i:end,:)-(tau*w)*(w'*R(i:end,:));
        Q(:,i:end) = Q(:,i:end)-(Q(:,i:end)*w)*(tau*w)';
        costo = costo + 3;
    end
    z = Q\b;
    raices = R\z; 
    error = norm(eye(m)-inv(Q*R)*A);
    costo = costo + 1;
end