% ==== DEFINICION GENERAL ==== %
clc; clear all; format long;
tol = 10^-16;

% ==== LOAD FILES ==== %

% ==== 289x289 ==== %
A1 = load('A289.dat');
b1 = load('b289.dat');

tic
[SolucJacoM1, ErrorJacoM1,Cont1JacoM1] = gaussJacobi(A1, b1, tol);
tiempoJ1 = toc;
tic
[SolucSeiM1, ErrorSeiM1,Cont1SeiM1] = gaussSeidel(A1, b1, tol);
tiempoS1 = toc;
tic
[valorDooM1, ErrorDooM1, Cont1DooM1] = doolittle(A1, b1);
tiempoD1 = toc;
tic
[valorHouM1, ErrorHouM1, Cont1HouM1] = Housholder(A1,b1);
tiempoHou1 = toc;
tic
[valorChoM1, ErrorChoM1, Cont1ChoM1] = cholesky(A1, b1);
tiempoC1 = toc;
tic
[valorQRM1, ErrorQRM1, Cont1QRM1] = QR(A1, b1);
tiempoQ1 = toc;


% ==== 1089x1089 ==== %

A1 = load('A1089.dat');
b1 = load('b1089.dat');

tic
[SolucJacoM2, ErrorJacoM2, valorJacoM2,Cont1JacoM2,Cont2JacoM2] = gaussJacobi(A1, b1, tol);
tiempoJ1 = toc;
tic
[SolucSeiM2, ErrorSeiM2, valorSeiM2,Cont1SeiM2,Cont2SeiM2] = gaussSeidel(A1, b1, tol);
tiempoS1 = toc;
tic
[valorDooM2, ErrorDooM2, Cont1DooM2,Cont2DooM2] = doolittle(A1, b1);
tiempoD1 = toc;
tic
[valorHouM2, ErrorHouM2, Cont1HouM2,Cont2HouM2] = Housholder(A1,b1);
tiempoHou1 = toc;
tic
[valorChoM2, ErrorChoM2, Cont1ChoM2,Cont2ChoM2] = cholesky(A1, b1);
tiempoC1 = toc;
tic
[valorQRM2, ErrorQRM2, Cont1QRM2,Cont2QRM2] = QR(A1, b1);
tiempoQ1 = toc;

% ==== 4225x4225 ==== %

A1 = load('A4225.dat');
b1 = load('b4225.dat');

tic
[SolucJacoM1, ErrorJacoM1, valorJacoM1,Cont1JacoM1,Cont2JacoM1] = gaussJacobi(A1, b1, tol);
tiempoJ1 = toc;
tic
[SolucSeiM1, ErrorSeiM1, valorSeiM1,Cont1SeiM1,Cont2SeiM1] = gaussSeidel(A1, b1, tol);
tiempoS1 = toc;
tic
[valorDooM1, ErrorDooM1, Cont1DooM1,Cont2DooM1] = doolittle(A1, b1);
tiempoD1 = toc;
tic
[valorHouM1, ErrorHouM1, Cont1HouM1,Cont2HouM1] = Housholder(A1,b1);
tiempoHou1 = toc;
tic
[valorChoM1, ErrorChoM1, Cont1ChoM1,Cont2ChoM1] = cholesky(A1, b1);
tiempoC1 = toc;
tic
[valorQRM1, ErrorQRM1, Cont1QRM1,Cont2QRM1] = QR(A1, b1);
tiempoQ1 = toc;