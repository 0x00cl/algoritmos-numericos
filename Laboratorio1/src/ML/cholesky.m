function [raices,error,costo] = cholesky(A, b)
    [m, n]=size(A);
    costo = 0;
    for k = 1 : m
        A(k,k) = sqrt(A(k,k));
        A(k+1:m,k) = A(k+1:m,k)/A(k,k);
        for j = k + 1 : m
            A(j:m,j) = A(j:m,j) - A(j,k)*A(j:m,k);
            costo = costo + 1;
        end
        costo = costo + 1;
    end
    L = tril(A);
    LT = L';
    z = L\b;
    raices = LT\z;
    error = norm(eye(m)-inv(L*LT)*A);
    costo = costo + 1;
end
