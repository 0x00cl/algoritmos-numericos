function [raices,error,costo] = QR(A, b)
    [m, n] = size(A);
    R = zeros(n, n);
    V = A;
    Q = zeros(m, n);
    costo = 0;
    raices=[];
    for i = 1: n
        R(i, i) = norm(V(:, i));
        Q(:, i) = V(:, i)/R(i, i);
        for j = i + 1: m
           R(i, j)= (Q(:, i)')*V(:, j);
           V(:, j)=V(:, j) - R(i, j)*Q(:, i);
           costo = costo + 1 ;
        end
    end
    raices = [raices, inv(R)*Q'*b];
    error = norm(eye(m) - inv(Q*R)*A);
    costo = costo + 1;
end