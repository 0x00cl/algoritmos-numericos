function [raices, errores, costo] = gaussJacobi(A, b, tol)
    [m, n] = size(A);
    D = zeros(m, n);
    E = zeros(m, n);
    F = zeros(m, n);
    aux = 1;
    costo = 0;
    for i = 1 : m
       D(i, i) = A(i, i);
       for j = 1 : aux
          E(i, j) = A(i, j); 
          F(j, i) = A(j, i);
       end
       E(i, i) = 0;
       F(i, i) = 0;
       aux = aux + 1;
       costo = costo + 1;
    end
    x0 = zeros(n ,1);
    raices = [];
    errores = [];
    for i = 1 : 100
       j = inv(D)*(-E-F);
       c = D\b;
       x1 = j*x0 + c;
       e = norm(x1 - x0, inf)/norm(x1, inf);
       raices = [raices, x1];
       errores = [errores, e];
       if(e < tol)
           break
       end
       x0 = x1;
       costo = costo + 3;
    end
end