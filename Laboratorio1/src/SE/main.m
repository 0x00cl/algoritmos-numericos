% ==== DEFINICION GENERAL ==== %
clc; format long;
tol = 10^-16;
n = 100;

% ==== SISTEMAS DE ECUACIONES 1 ==== %
partida1 = [1,1];
syms x y
F1 = x^2 - 10*x + y^2 + 8;
F2 = x * y^2 + x - 10*y + 8;
tic
[raices1, errores1] = newtonRaphsonMultiVar([F1;F2], [x,y], partida1, tol, n);
tiempoNM2 = toc;
graphError(errores1)
% ==== SISTEMAS DE ECUACIONES 2 ==== %
partida2 = [1,1,1];
syms x y z
F3 = x^2 + y - 37;
F4 = x - y^2 - 5;
F5 = x + y + z - 3;
tic
[sistema2, errorNM3] = newtonRaphsonMultiVar([F3;F4;F5], [x,y,z], partida2, tol, n);
tiempoNM3 = toc;
graphError(errorNM3)