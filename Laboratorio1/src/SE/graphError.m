function graphError(errores)

    figure1 = figure;

    % Create axes
    axes1 = axes('Parent',figure1);
    box(axes1,'on');
    hold(axes1,'all');

    % Create plot
    plot([errores],'-o','MarkerSize',2); hold on;
    title('Errores sistema de ecuaciones')
    ylabel('Error') % y-axis label
    xlabel('Iter') % x-axis label

end