function [raices, errores] = newtonRaphsonMultiVar(funciones, incognitas, x0, tolerancia, n)
    H = jacobian(funciones,incognitas);
    X = x0;
    raices = [];
    errores = [];
    for i = 1: n
        F_X = subs(funciones,incognitas,X);
        F_prime_X = subs(H,incognitas,X);
        jacoInv = inv(F_prime_X);
        x1 = x0 - (jacoInv*F_X);
        raices = [raices, x1];
        error = norm(x1 - x0, inf)/norm(x1, inf);
        errores = [errores, error];
        x0 = x1;
        if (error < tolerancia)
            break;
        end
    end
end